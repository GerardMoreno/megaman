grav = 0.2;
hsp = 0;
vsp = 0;

speed_carry = 0;
platform = false;

jumpspeed_normal = 4;
jumpspeed_powerup = 5;
movespeed = 4;
jumpspeed = jumpspeed_normal;

//IA Checks
hcollision = false;
dir = 0;
fearofheights = false;

//Constants
grounded = false;
jumping = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 4;
hsp_jump_applied = 0;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;

//Init variables
key_left = 0;
key_right = 0;
key_jump = false;
key_shoot = false;
